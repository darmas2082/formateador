package com.formateador.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.formateador.model.Prestamos;
import com.formateador.model.SocioVO;
import com.formateador.model.TransferenciaVO;
import com.formateador.model.listaFPSResponse;
import com.formateador.util.Constantes;


@RestController
public class FPSController {

	
	
	@Value("${config.ruta-crol.url}")
	private String rutaConfigCrol;
	
	@PostMapping("/listarfps")
	public Map<String, Object> obtenerFPS(@RequestBody Prestamos prestamos){
		Map<String, Object> respuesta=new HashMap<>();
		listaFPSResponse listaR=new listaFPSResponse();
		try {
			RestTemplate restTemplate=new RestTemplate();
			listaR=restTemplate.postForObject(rutaConfigCrol+"/listarfps",prestamos, listaFPSResponse.class); 
			listaR.setSuccess(true);
			respuesta.put("data", listaR);
			respuesta.put("success", true);
		}catch(Exception ex) {
			respuesta.put("success", false);
			respuesta.put("message", "Ocurrió un problema al obtener: "+ ex.getMessage());
			
		}
		return respuesta;
	}
	
	
	@PostMapping("/deudaFPS")
	public Map<String, Object> obtenerDeuda(@RequestBody SocioVO socio) throws Exception{
		Map<String, Object> fpsResponse=new HashMap<>();
		try {
			RestTemplate restTemplate=new RestTemplate();
			fpsResponse=restTemplate.postForObject(rutaConfigCrol+"/deudaFPS", socio,HashMap.class);
			return fpsResponse;
		}catch(Exception e) {
			fpsResponse.put("success", false);
			fpsResponse.put("mensaje", e.getMessage());
			return fpsResponse;
		}
	}
	
	@PostMapping("/pagoFPS")
	public TransferenciaVO transferenciaPropias(@RequestBody TransferenciaVO transferencia ){
		TransferenciaVO transferenciaR=new TransferenciaVO();
		try {
			RestTemplate restTemplate=new RestTemplate();
			transferenciaR=restTemplate.postForObject(rutaConfigCrol+"/pagoFPS",transferencia, TransferenciaVO.class); 
			transferenciaR.setSuccess(transferenciaR.isSuccess());
		}catch(Exception ex) {
			transferenciaR.setSuccess(false);
			transferenciaR.setDetalleRespuesta("Ocurrió un problema al obtener: "+ ex.getMessage());
		}
		return transferenciaR;
	}
}