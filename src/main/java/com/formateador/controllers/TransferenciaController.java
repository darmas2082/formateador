package com.formateador.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.formateador.model.TransferenciaVO;
import com.formateador.util.Constantes;



@RestController
public class TransferenciaController {
	
	@Value("${config.ruta-crol.url}")
	private String rutaConfigCrol;
	
	@PostMapping("/transferencia-propia")
	public TransferenciaVO transferenciaPropias(@RequestBody TransferenciaVO transferencia ){
		TransferenciaVO transferenciaR=new TransferenciaVO();
		try {
			RestTemplate restTemplate=new RestTemplate();
			transferenciaR=restTemplate.postForObject(rutaConfigCrol+"/transferencia-propia",transferencia, TransferenciaVO.class); 
			transferenciaR.setSuccess(transferenciaR.isSuccess());
		}catch(Exception ex) {
			transferenciaR.setSuccess(false);
			transferenciaR.setDetalleRespuesta("Ocurrió un problema al obtener: "+ ex.getMessage());
		}
		return transferenciaR;
	}
}