package com.formateador.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.formateador.model.listaAhorrosResponse;
import com.formateador.model.listaFPSResponse;
import com.formateador.rest.ServicioCrolFeign;
import com.formateador.util.Constantes;

@RestController
public class AhorroController {

		
	@Value("${config.ruta-crol.url}")
	private String rutaConfigCrol;
	
	@PostMapping("/ahorros")
	public listaAhorrosResponse obtenerAhorroPost(@RequestBody listaAhorrosResponse ahorro){
		
		
		listaAhorrosResponse listaAhorrosR=new listaAhorrosResponse();
		try {
			
			RestTemplate restTemplate=new RestTemplate();
			listaAhorrosR=restTemplate.postForObject(rutaConfigCrol+"/ahorros", ahorro,listaAhorrosResponse.class);
			listaAhorrosR.setSuccess(true);
			
		}catch(Exception ex) {
			listaAhorrosR.setSuccess(false);
			listaAhorrosR.setMensaje(ex.getMessage());
			
		}
		return listaAhorrosR;
	}
}
