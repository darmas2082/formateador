package com.formateador.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.formateador.model.MovimientoResponse;
import com.formateador.model.MovimientoVO;
import com.formateador.util.Constantes;

@RestController
public class MovimientosAhorroController {

	
	
	@Value("${config.ruta-crol.url}")
	private String rutaConfigCrol;
	
	@PostMapping("/movimientosahorros")
	public MovimientoResponse obtenerAhorro(@RequestBody MovimientoVO movimiento){
		
		MovimientoResponse movimientosR=new MovimientoResponse();
		try {
			RestTemplate restTemplate=new RestTemplate();
			movimientosR=restTemplate.postForObject(rutaConfigCrol+"/movimientosahorros",movimiento, MovimientoResponse.class); 
			movimientosR.setSuccess(true);
		}catch(Exception ex) {
			movimientosR.setSuccess(false);
			movimientosR.setMensaje("Ocurrió un problema al obtener: "+ ex.getMessage());
			
		}
		return movimientosR;
	}
}
