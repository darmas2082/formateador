package com.formateador.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.formateador.model.PrestamosVO;
import com.formateador.model.SocioVO;
import com.formateador.model.TransferenciaVO;
import com.formateador.model.listaPrestamosResponse;
import com.formateador.util.Constantes;

@RestController
public class PrestamosController {
	
	@Autowired
	private Constantes constantes;
	
	@Value("${config.ruta-crol.url}")
	private String rutaConfigCrol;
	
	@PostMapping("/prestamos")
	public listaPrestamosResponse validaPin(@RequestBody SocioVO socioVO){
		
		listaPrestamosResponse listaPrestamosR=new listaPrestamosResponse();
		try {
			RestTemplate restTemplate=new RestTemplate();
			listaPrestamosR=restTemplate.postForObject(rutaConfigCrol+"/prestamos",socioVO, listaPrestamosResponse.class); 
			listaPrestamosR.setSuccess(listaPrestamosR.isSuccess());
		}catch(Exception ex) {
			listaPrestamosR.setSuccess(false);
			listaPrestamosR.setMensaje("Ocurrió un problema al obtener: "+ ex.getMessage());
			
		}
		return listaPrestamosR;
	}
	
	
	@PostMapping("/movimientoPrestamo")
	public PrestamosVO validaPin(@RequestBody PrestamosVO prestamoVO){
		
		PrestamosVO listaPrestamosR=new PrestamosVO();
		try {
			RestTemplate restTemplate=new RestTemplate();
			listaPrestamosR=restTemplate.postForObject(rutaConfigCrol+"/movimientoPrestamo",prestamoVO, PrestamosVO.class); 
			listaPrestamosR.setSuccess(listaPrestamosR.getSuccess());
		}catch(Exception ex) {
			listaPrestamosR.setSuccess(false);
			listaPrestamosR.setMensaje("Ocurrió un problema al obtener: "+ ex.getMessage());
			
		}
		return listaPrestamosR;
	}
	
	@PostMapping("/prestamoPendiente")
	public listaPrestamosResponse obtenerPrestamosPendiente(@RequestBody PrestamosVO prestamosVO) {
		listaPrestamosResponse listaPrestamosR=new listaPrestamosResponse();
		try {
			RestTemplate restTemplate=new RestTemplate();
			listaPrestamosR=restTemplate.postForObject(rutaConfigCrol+"/prestamoPendiente",prestamosVO, listaPrestamosResponse.class); 
			listaPrestamosR.setSuccess(listaPrestamosR.isSuccess());
		}catch(Exception ex) {
			listaPrestamosR.setSuccess(false);
			listaPrestamosR.setMensaje("Ocurrió un problema al obtener: "+ ex.getMessage());
			
		}
		return listaPrestamosR;
		
	}
	
	@PostMapping("/pagoPrestamo")
	public TransferenciaVO transferenciaPropias(@RequestBody TransferenciaVO transferencia ){
		TransferenciaVO transferenciaR=new TransferenciaVO();
		try {
			RestTemplate restTemplate=new RestTemplate();
			transferenciaR=restTemplate.postForObject(rutaConfigCrol+"/pagoPrestamo",transferencia, TransferenciaVO.class); 
			transferenciaR.setSuccess(transferenciaR.isSuccess());
		}catch(Exception ex) {
			transferenciaR.setSuccess(false);
			transferenciaR.setDetalleRespuesta("Ocurrió un problema al obtener: "+ ex.getMessage());
		}
		return transferenciaR;
	}

}
