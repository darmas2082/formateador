package com.formateador.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.formateador.model.SocioVO;
import com.formateador.model.listaAhorrosResponse;
import com.formateador.model.loginResponse;
import com.formateador.rest.ServicioCrolFeign;
import com.formateador.util.Constantes;

@RestController
public class LoginController {

	
	
	@Value("${config.ruta-crol.url}")
	private String rutaConfigCrol;
	
	
	@PostMapping("/login")
	public Map<String, Object> obtenerAhorro(@RequestBody SocioVO socio){
		Map<String, Object> respuesta=new HashMap<>();
		SocioVO loginR=new SocioVO();
		try {
			RestTemplate restTemplate=new RestTemplate();
			loginR=restTemplate.postForObject(rutaConfigCrol+"/login",socio, SocioVO.class); 
			
			loginR.setSuccess(true);
			respuesta.put("data", loginR);
			respuesta.put("success", true);
		}catch(Exception ex) {
			respuesta.put("success", false);
			respuesta.put("message", "Ocurrió un problema al obtener: "+ ex.getMessage());
			
		}
		return respuesta;
	}
}
