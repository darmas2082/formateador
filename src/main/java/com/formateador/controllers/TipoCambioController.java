package com.formateador.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.formateador.model.TipoCambio;
import com.formateador.util.Constantes;



@RestController
public class TipoCambioController {
	
	@Value("${config.ruta-crol.url}")
	private String rutaConfigCrol;
	
	@PostMapping("/tipoCambio")
	public TipoCambio obtenerTipoCambio(@RequestBody TipoCambio tipoCambio ){
		
		try {
			
			RestTemplate restTemplate=new RestTemplate();
			tipoCambio=restTemplate.postForObject(rutaConfigCrol+"/tipoCambio", tipoCambio,TipoCambio.class);
			tipoCambio.setSuccess(tipoCambio.isSuccess());
			
		}catch(Exception ex) {
			tipoCambio.setSuccess(false);
			tipoCambio.setMensaje(ex.getMessage());
			
		}
		return tipoCambio;
	}
}
