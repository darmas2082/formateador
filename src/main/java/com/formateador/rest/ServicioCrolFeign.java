package com.formateador.rest;

//import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.formateador.model.listaAhorrosResponse;
import com.formateador.model.listaFPSResponse;
import com.formateador.model.loginResponse;


//@FeignClient(value = "servicio-crol")
public interface ServicioCrolFeign {
	@GetMapping("/login/{nroTarjeta}/{pin}/{token}/{callback}")
	public loginResponse obtener(@PathVariable("nroTarjeta") String nroTarjeta,
			@PathVariable("pin") String pin,@PathVariable("token") String token,@PathVariable("callback") String callback);
	
	@GetMapping("/listarfps/{idTercero}/{token}/{callback}")
	public listaFPSResponse obtener(@PathVariable("idTercero") String idTercero,
			@PathVariable("token") String token,@PathVariable("callback") String callback);
	
	@GetMapping("/ahorros/{codigo}/{token}/{callback}")
	public listaAhorrosResponse obtenerAhorro(@PathVariable("ahorros") String codigo,
			@PathVariable("token") String token,
			@PathVariable("callback") String callback);
}
