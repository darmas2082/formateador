package com.formateador.model;

public class TipoCambio {

	private Integer monedaOrigen;
	private Integer monedaDestino;
	private double tipoCambio;
	private boolean success;
	private String mensaje;
	private String token;
	private String codigo;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Integer getMonedaOrigen() {
		return monedaOrigen;
	}
	public void setMonedaOrigen(Integer monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}
	public Integer getMonedaDestino() {
		return monedaDestino;
	}
	public void setMonedaDestino(Integer monedaDestino) {
		this.monedaDestino = monedaDestino;
	}
	public double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}
