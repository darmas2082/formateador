package com.formateador.model;

import java.util.List;



public class listaAhorrosResponse {
	public String codigo;
	public String mensaje;
	public List<Ahorros> cuentas;
	public Boolean success;
	public String token;
	
	
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public List<Ahorros> getCuentas() {
		return cuentas;
	}
	public void setCuentas(List<Ahorros> cuentas) {
		this.cuentas = cuentas;
	}
	
	
}