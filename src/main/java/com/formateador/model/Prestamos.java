package com.formateador.model;

import java.util.Date;

public class Prestamos {
	public String Fecha;
	public String Descripcion;
	public String Monto;
	public String Voucher;
	public String idTercero;
	public String token;
	

	public Date fecVigencia;
	public Date fecVencimiento;
	public String condicion;
	
	
	
	
	
	public String getIdTercero() {
		return idTercero;
	}
	public void setIdTercero(String idTercero) {
		this.idTercero = idTercero;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Date getFecVigencia() {
		return fecVigencia;
	}
	public void setFecVigencia(Date fecVigencia) {
		this.fecVigencia = fecVigencia;
	}
	public Date getFecVencimiento() {
		return fecVencimiento;
	}
	public void setFecVencimiento(Date fecVencimiento) {
		this.fecVencimiento = fecVencimiento;
	}
	public String getCondicion() {
		return condicion;
	}
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	public String getFecha() {
		return Fecha;
	}
	public void setFecha(String fecha) {
		Fecha = fecha;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public String getMonto() {
		return Monto;
	}
	public void setMonto(String monto) {
		Monto = monto;
	}
	public String getVoucher() {
		return Voucher;
	}
	public void setVoucher(String voucher) {
		Voucher = voucher;
	}
	
}
