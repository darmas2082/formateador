package com.formateador.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class Constantes {

	@Value("${config.ruta-crol.url}")
	private String rutaConfigCrol;
	
	public  String rutaCrol=rutaConfigCrol;
}
